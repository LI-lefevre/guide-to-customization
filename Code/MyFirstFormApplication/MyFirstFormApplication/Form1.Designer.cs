﻿namespace MyFirstFormApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HelloWorldButton = new System.Windows.Forms.Button();
            this.MyNewComboBox = new System.Windows.Forms.ComboBox();
            this.ActcutIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ActcutIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // HelloWorldButton
            // 
            this.HelloWorldButton.Location = new System.Drawing.Point(210, 94);
            this.HelloWorldButton.Name = "HelloWorldButton";
            this.HelloWorldButton.Size = new System.Drawing.Size(158, 23);
            this.HelloWorldButton.TabIndex = 0;
            this.HelloWorldButton.Text = "my text button";
            this.HelloWorldButton.UseVisualStyleBackColor = true;
            this.HelloWorldButton.Click += new System.EventHandler(this.HelloWorldButton_Click);
            // 
            // MyNewComboBox
            // 
            this.MyNewComboBox.FormattingEnabled = true;
            this.MyNewComboBox.Items.AddRange(new object[] {
            "Myfirst items",
            "second",
            "third"});
            this.MyNewComboBox.Location = new System.Drawing.Point(210, 161);
            this.MyNewComboBox.Name = "MyNewComboBox";
            this.MyNewComboBox.Size = new System.Drawing.Size(121, 21);
            this.MyNewComboBox.TabIndex = 1;
            this.MyNewComboBox.SelectedIndexChanged += new System.EventHandler(this.MyNewComboBox_SelectedIndexChanged);
            // 
            // ActcutIcon
            // 
            this.ActcutIcon.Location = new System.Drawing.Point(538, 161);
            this.ActcutIcon.Name = "ActcutIcon";
            this.ActcutIcon.Size = new System.Drawing.Size(71, 72);
            this.ActcutIcon.TabIndex = 2;
            this.ActcutIcon.TabStop = false;
            this.ActcutIcon.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ActcutIcon);
            this.Controls.Add(this.MyNewComboBox);
            this.Controls.Add(this.HelloWorldButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ActcutIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button HelloWorldButton;
        private System.Windows.Forms.ComboBox MyNewComboBox;
        private System.Windows.Forms.PictureBox ActcutIcon;
    }
}

