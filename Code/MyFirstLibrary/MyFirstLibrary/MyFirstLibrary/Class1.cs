﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wpm.Implement.Manager;
using Wpm.Implement.Processor;
using Wpm.Implement.ComponentEditor;
using Actcut.ActcutModelManager;
using Wpm.Schema.Kernel;
using Alma.BaseUI.ErrorMessage;
using Alma.UI.Dxf;
using Actcut.ResourceManager;
using Actcut.ActcutModelHelper;
using Actcut.AssemblyModelManager;
using Alma.BaseUI.Import;
// now I make almaCAM works with the new one code
namespace AlmaCamTraining
{
    public class MyAfterSendToWorkshopEvent : AfterSendToWorkshopEvent
    {
        public override void OnAfterSendToWorkshopEvent(IContext context, AfterSendToWorkshopArgs args) // function to get the information of the nesting
        {


            // creation of our dictionary, in this case, 
            // we'll send to our dictionary an IEntity  of nested parts and 
            //it will return us a string with the names of each reference
            IDictionary<IEntity, string> dictReferenceName = new Dictionary<IEntity, string>(); // creation of our dictionary, in this case, we'll send to our dictionary an IEntity list of nested parts and it will return us a string with their names


            long NestSendToCutID = args.NestingEntity.Id; // object ceated to get the argumentes of the nesting ID
            IEntity myNesting = args.NestingEntity;

            IEntityList PartNestedList = context.EntityManager.GetEntityList("_NESTED_REFERENCE", "_NESTING", ConditionOperator.Equal, NestSendToCutID); //TODO improve
            PartNestedList.Fill(false);

            // print each name of nested part
            List<string> TwoDpartNameStringList = new List<string>(); // list of string initializing (improvment)
            foreach (IEntity PartNested in PartNestedList)// 
            {

                string TwoDpartName = GetReferenceNameFromNestedPart(dictReferenceName, PartNested, out string errorMessage);
                TwoDpartNameStringList.Add(TwoDpartName);

            }
            // printing the list of strings(references name) was seted before
            string ConcataneTwoDpartNameStringList = string.Join(Environment.NewLine, TwoDpartNameStringList.ToArray());
            MessageBox.Show(ConcataneTwoDpartNameStringList);



            GetTotalQtyOfNestedParts(PartNestedList);
        }



        public int GetTotalQtyOfNestedParts(IEntityList PartNestedList)
        {


            try
            {
                int TotalQtyNestedPart = 0;
                foreach (IEntity PartNested in PartNestedList)
                {
                    TotalQtyNestedPart += PartNested.GetFieldValueAsInt("_QUANTITY");
                }
                return (TotalQtyNestedPart);
            }
            catch (Exception ex)
            {
                return -1;

            }
        }


        // function criated to send the informations to our dictionary
        private string GetReferenceNameFromNestedPart(IDictionary<IEntity, string> dictReferenceName, IEntity nestedpart, out string errorMessageList)
        {
            errorMessageList = string.Empty;

            string ReferenceName = null;
            bool youFindTheName = dictReferenceName.TryGetValue(nestedpart, out ReferenceName);
            if (youFindTheName == false)
            {
                IEntity TwoDpart = nestedpart.GetFieldValueAsEntity("_REFERENCE");
                ReferenceName = TwoDpart.GetFieldValueAsString("_NAME");
                dictReferenceName.Add(nestedpart, ReferenceName);
            }
            if (ReferenceName == null)
                errorMessageList = "There are no parts" + nestedpart.DefaultValue;
            return ReferenceName;
        }
    }

    public class SendAColorToCut : CommandProcessor
    {
        public override bool Execute()
        {


            //retreive all parameters given in the function
            int Param1 = RetreiveParameterFunction("PARAM1");
            int Param2 = RetreiveParameterFunction("PARAM2");
            MessageBox.Show("First param =" + Param1 + ", second param = " + Param2);
            bool IsCutManSelected = CutManSelector("CUT_MAN_FREE", out IEntity CutManEntity);

            bool IsFieldCutManSeted = SetinCutManField(IsCutManSelected, CutManEntity);

            //Rainbow myRainbow = new Rainbow();
            //myRainbow.SendColor(Context, Param1);

            //CreateReferenceFromMaterial();
            //ChangeReferenceMaterial("My reference");

            //sendtocutWithDescription();// rafa mission acomplish

            //CreateReferenceFromDxf();
            CreateReferenceFromTopo2D();
            EditMachinablePartInDrafter();

            // ImportCSVFromnewAPI();
            // GetMachineGap();

            return true;
        }

        private void ImportCSVFromnewAPI()
        {
            //AssemblyManager assemblyManager = new AssemblyManager();
            //ActcutAssemblyType actcutAssemblyType = ActcutAssemblyType.Cam;

            //string assemblyName = null;
            //string rootNodeKey = null;
            //string rootNodeName = null;
            //IActcutAssembly actcutAssembly = assemblyManager.CreateAssembly(Context, actcutAssemblyType, assemblyName, rootNodeKey, rootNodeName);


            //IEnumerable<string> filenameList = new string[] { "chemin1", "chemin2" };
            //ImportAssemblyListData importAssemblyListData = new ImportAssemblyListData(actcutAssemblyType, filenameList);
            //importAssemblyListData.CsvFilename = "mon chemin du csv";
            //assemblyManager.ImportAssemblyOutProcess(Context, importAssemblyListData, out ImportAssemblyResultList importAssemblyResultList);


            //CsvImporter csvImporter = new CsvImporter();
            //string myKey = null;
            //string myCaption = null;
            //CsvImporterField myfield = new CsvImporterField(myKey, myCaption);
            //csvImporter.AddField(myfield);
            //Func<ICsvImporterRow, string> buildKeyFunc = null;
            //csvImporter.Import(buildKeyFunc);

            //ICsvImporterResult csvImporterResult = new CsvImporterResult();

            CsvImporterDescription csvImporterDescription = new CsvImporterDescription();
            csvImporterDescription.Semicolon = true;
            csvImporterDescription.BeginLine = 2;
            CsvImporterField myField = new CsvImporterField("_REFERENCE", "2d part name");
            csvImporterDescription.AddField(myField);
            string fileName = @"C:\Users\lefevre\Desktop\demo_fallgatter\Fallgatter-Raphael-dev\Fallgatter-Raphael-dev\Test CSV-Files\importTest.csv";

            //you have to fill it
            ICsvImporterResult

                        result = csvImporterDescription.Import(fileName);
        }

        public void EditMachinablePartInDrafter()
        {
            // Get the machine Entity
            IEntity machine = GetFirstEntityFromKey("_CUT_MACHINE_TYPE");

            // get machine info
            MachineManager machineManager = new MachineManager();
            ICutMachine cutMachine = machineManager.GetCutMachine(Context, machine.Id);
            ICutMachineInfo cutMachineInfo = cutMachine.CutMachineInfo;

            // get one reference
            IEntity referenceEntity = GetFirstEntityFromKey("_REFERENCE");

            // get the material of the reference
            IEntity material = referenceEntity.GetFieldValueAsEntity("_MATERIAL");
            IEntity cutMachinecondition = cutMachineInfo.GetDefaultCondition(material);

            // open drafter
            DrafterModule drafterModule = new DrafterModule();
            drafterModule.Init(Context.Model.Id32, cutMachine.CutMachineInfo.CutMachineEntity.Id32);

            // create machinable part -> preparação
            IActcutReferenceManager actcutReferenceManager = ActcutReferenceManager.Create();
            IEntity machineblepart = actcutReferenceManager.CreateMachinablePart(Context, referenceEntity, cutMachine.CutMachineInfo.CutMachineEntity, cutMachinecondition, "Mymachinablepart", 0, false);

            //drawing the part
            drafterModule.OpenMachinablePart(CamObjectType.CamObject, machineblepart.Id32);
            drafterModule.NewProfile(2, 1, 0, 0, 0);
            drafterModule.AddElement(100, 0, 0, 0, 0);
            drafterModule.AddElement(100, 100, 0, 0, 0);
            drafterModule.AddElement(0, 100, 0, 0, 0);
            drafterModule.AddElement(0, 0, 0, 0, 0);
            drafterModule.EndProfile();
            drafterModule.SaveMachinablePart(true);
        }

        private IEntity GetFirstEntityFromKey(string key)
        {
            try
            {
                IEntityList entity_list = Context.EntityManager.GetEntityList(key);
                entity_list.Fill(false);
                IEntity entity_to_retun = entity_list.FirstOrDefault();
                return entity_to_retun;

            }
            catch (Exception ex)
            {
                //the function GetFirstEntityFromKey failed 
                //what are we suppose to do?
                return null;
            }
        }

        private void GetMachineGap()
        {
            //******************************************************************
            //6
            // acceder aux paramètres machine

            //IEntityList machineEntityList = Context.EntityManager.GetEntityList("_CUT_MACHINE_TYPE", "_NAME", ConditionOperator.Equal, "Mazak_Optiplex 3015_4KW_FibreII");
            //machineEntityList.Fill(false);
            //IEntity machineEntity = machineEntityList.FirstOrDefault();

            //IEntitySelector materialSelector = new EntitySelector();
            //IMachineManager machineManager = new MachineManager();
            //ICutMachine cutMachine = machineManager.GetCutMachine(machineEntity);

            //IList<IEntity> materialEntityList = new List<IEntity>(cutMachine.CutMachineInfo.AvailableMaterialList);
            //IExtendedEntityList exEntityList = Context.EntityManager.GetExtendedEntityList(Context.Kernel.GetEntityType("_MATERIAL"), materialEntityList);
            //exEntityList.Fill(false);

            //materialSelector.Init(Context, exEntityList);
            //if (materialSelector.ShowDialog() == DialogResult.OK)
            //{
            //    IEntity materialEntity = materialSelector.SelectedEntity.First();
            //    IEntity defaultConditionEntity = cutMachine.CutMachineInfo.GetDefaultCondition(materialSelector.SelectedEntity.First());

            //    ICutMachineResource cutMachineRessource = cutMachine.GetCutMachineResource(false);

            //    cutMachineRessource.GetParameterValue("STANDARD_GAP", materialEntity, defaultConditionEntity);


            //}
            //******************************************************************
        }

        private void CreateReferenceFromTopo2D()
        {
            //******************************************************************
            //3
            //création d'une part, avec topo


            //IEntitySelector materialSelector = new EntitySelector();
            //materialSelector.Init(Context, Context.Kernel.GetEntityType("_MATERIAL"));
            //material


            //if(materialSelector.ShowDialog()==DialogResult.OK)
            //{
            //    IActcutReferenceManager actcutReferecneManager = new ActcutReferenceManager();
            //    double dx = 500;
            //    double dy = 200;


            //    Topo2d.ProfilePtr profil = Topo2d.NewProfileXY(0, 0);
            //    Topo2d.AddSegmentXY(profil, dx, 0);
            //    Topo2d.AddSegmentXY(profil, dx, dy);
            //    Topo2d.AddSegmentXY(profil, 0, dy);
            //    Topo2d.AddSegmentXY(profil, 0, 0);
            //    Topo2d.CloseProfile(profil);


            //Topo2d.FacePtr face = Topo2d.NewFace(profile);

            //Topo2d.PartPtr part = Topo2d.NewPart();
            //Topo2d.AddFace(part, face);

            //IEntity machinablePart;
            //ActcutReferenceManager.CreateReferenceFromTopo(Context, "Topo ref", Topo2d.ConvertPart(part), materialselector.SelectedEntity.First(), machineEntity, conditionEntity, out machinablePart);

            //}

        }

        private void CreateReferenceFromDxf()
        {
            //******************************************************************
            //2
            //create reference from dxf

            #region select machine
            IEntityList machineEntityList = Context.EntityManager.GetEntityList("_CUT_MACHINE_TYPE");
            machineEntityList.Fill(false);
            IEntity machineEntity = machineEntityList.FirstOrDefault();
            #endregion

            #region selection of cutting condition
            IEntityList conditionEntityList = Context.EntityManager.GetEntityList("_CUT_MACHINE_CONDITION");
            conditionEntityList.Fill(false);
            IEntity conditionEntity = conditionEntityList.FirstOrDefault();
            #endregion

            #region select the material
            IEntitySelector materialSelector = new EntitySelector();
            materialSelector.Init(Context, Context.Kernel.GetEntityType("_MATERIAL"));
            #endregion
            if (materialSelector.ShowDialog() == DialogResult.OK)
            {
                #region create or select the dxfConfiguration
                IDxfConfigurationList dxfConfigurationList = new DxfConfigurationList();
                IDxfConfiguration dxfConfiguration = null;
                if (dxfConfigurationList.Count() == 0)
                {
                    dxfConfiguration = dxfConfigurationList.Add();
                    IDxfFilter dxfFilter = dxfConfiguration.DxfFilterList.Add();
                    dxfFilter.Name = "0";

                    dxfFilter.DxfCamToolKey = "CUT";
                }
                else
                    dxfConfiguration = dxfConfigurationList.FirstOrDefault();

                #endregion

                #region create the part from dxf
                IActcutReferenceManager actcutReferenceManager = new ActcutReferenceManager();
                IEntity newReference = actcutReferenceManager.CreateReferenceFromDxf(
                    Context,
                    "My reference from dxf",
                    @"C:\Users\lefevre\Desktop\dxf path\Mydxf.dxf",
                    materialSelector.SelectedEntity.First(),
                    machineEntity,
                    conditionEntity,
                    dxfConfiguration,
                    out IEntity newMachinablePartEntity);
                #endregion
            }
        }

        public void CreateReferenceFromMaterial()
        {
            //******************************************************************
            //1
            //create a reference

            IEntitySelector materialSelector = new EntitySelector();
            materialSelector.Init(Context, Context.Kernel.GetEntityType("_MATERIAL"));
            if (materialSelector.ShowDialog() == DialogResult.OK)
            {
                IActcutReferenceManager actcutReferenceManager = new ActcutReferenceManager();
                IEntity newReference = actcutReferenceManager.CreateReference(Context, "My reference", materialSelector.SelectedEntity.First());
            }

            //******************************************************************
        }

        private bool ChangeReferenceMaterial(string ReferenceName)
        {
            #region 1. retreive the reference entity from the reference name

            IEntityList ReferenceEntityList = Context.EntityManager.GetEntityList("_REFERENCE", "_NAME", ConditionOperator.Equal, ReferenceName);
            ReferenceEntityList.Fill(false);
            IEntity ReferenceEntity = ReferenceEntityList.FirstOrDefault();


            //check if we retreive the reference
            if (ReferenceEntity == null)
                return false;

            #endregion

            #region 2. select new material
            IEntityList MatrialEntityList = Context.EntityManager.GetEntityList("_MATERIAL", "_NAME", ConditionOperator.Equal, "Aço 0.50 mm");
            MatrialEntityList.Fill(false);
            IEntity MatrialEntity = MatrialEntityList.FirstOrDefault();
            #endregion

            #region 3. change material
            IActcutReferenceManager actcutReferenceManager = new ActcutReferenceManager();

            ITransaction trans = Context.CreateQuickTransaction();//.CreateTransaction();
            ChangeReferenceMaterialData changeReferenceMaterial = new ChangeReferenceMaterialData();
            changeReferenceMaterial.DoPreparation = true;
            changeReferenceMaterial.MaterialId = MatrialEntity.Id;

            bool resultchangematerial = actcutReferenceManager.ChangeReferenceMaterial(trans, (IEnumerable<IEntity>)ReferenceEntityList, changeReferenceMaterial,
                 out IList<IEntity> modifiedMachinablePartEntityList, out IErrorMessageList errorMessageList);
            trans.Save();
            #endregion

            return true;

        }

        public int RetreiveParameterFunction(string keyOfParameter)
        {
            bool TryGetParamaResult = Context.ParameterSetManager.TryGetParameterValue(Command.CommandType.Key, keyOfParameter, out IParameterValue parameterValue);

            if (TryGetParamaResult == false) // equal (!TryGetParamaResult)  (TryGetParamaResult != true)
                return -1;
            return int.Parse(parameterValue.Value.ToString());
        }

        public bool SetinCutManField(bool IsCutManSelected, IEntity CutManEntity)
        {
            // if the getman fuction succed we will add it the field cutman in part to produce
            if (IsCutManSelected == true)
            {
                // comand used to get all selected parts in part to produce to add a cut man

                IEnumerable<IEntity> PartToProduceEntityList = Command.WorkOnEntityList;

                //loop created to take all selected parts, and set the field "CUT_MAN_TO_PRODUCE" with the value of CutManEntity

                foreach (IEntity PartToProduce in PartToProduceEntityList)
                {

                    PartToProduce.SetFieldValue("CUT_MAN_TO_PRODUCE", CutManEntity); //set the value in CutManEntity in the field "CUT_MAN_TO_PRODUCE"
                    PartToProduce.Save();

                }

            }
            else
            {
                //if the getman fection not succed stop the excecution 
                return false;
            }
            return true;
        }
        public bool CutManSelector(string Key, out IEntity CutManEntity)
        {
            CutManEntity = null;
            IEntitySelector CutManEntitySelector = new EntitySelector();
            CutManEntitySelector.Init(Context, Context.Kernel.GetEntityType(Key));
            CutManEntitySelector.MultiSelect = false;

            // if we selected one cutman then
            if (CutManEntitySelector.ShowDialog() == DialogResult.OK)
            {

                CutManEntity = CutManEntitySelector.SelectedEntity.FirstOrDefault();//select the cut man we selected
                if (CutManEntity == null) // if no part was selected the entity will be null, so it will return false and stop the exectuion
                    return false;
                else

                    return true; //of the entity are not null, we make the progres
            }


            return false;
        }

    }

    public class Rainbow
    {
        // process :
        // 1. we choose a color
        // 2. we retreive all parts with this color
        // 3. we create part to produce for each selected part
        public void SendColor(IContext myContext, int myParameter1)
        {
            #region 1.
            // Create an "EntitySelector" object to select the color we want
            IEntitySelector colorEntitySelector = new EntitySelector();

            colorEntitySelector.Init(myContext, myContext.Kernel.GetEntityType("COLOR_LIST"));
            //Open the "Color list" tab to allow a selection.
            colorEntitySelector.MultiSelect = false; // Only the last selected element will be taken into account

            // Open the window for selection and check if the user click on OK on this window
            if (colorEntitySelector.ShowDialog() == DialogResult.OK)
            {

                // The entity "colorSelected" contains the first (and the only one) element selected
                IEntity colorSelected = colorEntitySelector.SelectedEntity.FirstOrDefault();
                #endregion

                #region 2.
                //Load the list of reference present in the database in an IEntityList object 
                IEntityList partEntityList = myContext.EntityManager.GetEntityList("_REFERENCE");

                // Fill the list, parameter "false" means that binary data (preview for example) are not load (not necessary in our function, so we save time)
                partEntityList.Fill(false);

                #endregion

                #region 3.
                foreach (IEntity reference in partEntityList) // Loop on each reference in partEntityList
                {
                    // If (the color selected is the color of the part) or(the part doesn't have any color (so 0 by default) and PARAM1=0)
                    if (reference.GetFieldValueAsInt("PART_COLOR") == colorSelected.GetFieldValueAsInt("INT_ID") ||
                        (reference.GetFieldValueAsInt("PART_COLOR") == 0 && myParameter1 == 0)
                    )
                    {
                        // We create a part to produce
                        IEntity partToProduce = myContext.EntityManager.CreateEntity("_TO_PRODUCE_REFERENCE");

                        //we link it with the given reference (2D part)
                        partToProduce.SetFieldValue("_NAME", reference.GetFieldValueAsString("_NAME"));

                        // With a Quantity to produce equals to 1
                        partToProduce.SetFieldValue("_QUANTITY", 1);

                        // And we associate it to the corresponding reference
                        partToProduce.SetFieldValue("_REFERENCE", reference);

                        // Finally, we save the created part to produce
                        partToProduce.Save();
                    }
                }
                #endregion
            }
        }
    }
}
